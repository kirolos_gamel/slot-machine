

function RandomInt(low, high)
{
    return Math.floor(Math.random()*(high-low+1)) + low;
}


function RandomOneOf(list)
{
    return list[RandomInt(0, list.length-1)];
}




var spinCount = 0;
var images = ['3xBAR', 'BAR', '2xBAR', '7', 'Cherry'];
var position;

function TakeInput(){

	if(document.getElementById('center').checked || document.getElementById('top_bottom').checked){

       if(document.getElementById('center').checked ){
			SpinSlots("center");
			
			
		}
		else if(document.getElementById('top_bottom').checked){
			SpinSlots("top_bottom");
		}
		
	}
	else{
		var msgspan = document.getElementById("message");
        msgspan.innerHTML = "Please Choose One Wining Position.";
        msgspan.style.color = "red";
        console.log(msgspan);  
	}

}

function SpinSlots(position)
      
{
	// if not spinning, pick a random number of spins
	if ( spinCount == 0 ) spinCount = 10 + Math.floor( 20 * Math.random() );

	var slot1 = RandomOneOf(images);
	var slot2 = RandomOneOf(images);
	var slot3 = RandomOneOf(images);
	var slot4 = RandomOneOf(images);
	var slot5 = RandomOneOf(images);
	var slot6 = RandomOneOf(images);
	var slot7 = RandomOneOf(images);
	var slot8 = RandomOneOf(images);
	var slot9 = RandomOneOf(images);

	document.getElementById('slot1Img').src =
	'images/' + slot1 + '.png';
	document.getElementById('slot2Img').src =
	'images/' + slot2 + '.png';
	document.getElementById('slot3Img').src =
	'images/' + slot3 + '.png';

	document.getElementById('slot4Img').src =
	'images/' + slot4 + '.png';
	document.getElementById('slot5Img').src =
	'images/' + slot5 + '.png';
	document.getElementById('slot6Img').src =
	'images/' + slot6 + '.png';

	document.getElementById('slot7Img').src =
	'images/' + slot7 + '.png';
	document.getElementById('slot8Img').src =
	'images/' + slot8 + '.png';
	document.getElementById('slot9Img').src =
	'images/' + slot9 + '.png';

        --spinCount;
        if ( spinCount > 0 )
        {
            setTimeout(function() {
              SpinSlots(position);
            }, 50);
            return;
        }

	// assume the person loses:	
        var credits = parseInt(document.getElementById('credits').innerHTML);
        credits -=10; // always costs 10 credit to play

	var msg = "You lose!";
        var color = "red";


    if(position == "top_bottom"){


 
        if (slot1 == slot2 && slot2 == slot3)
		{

		    if(slot1=="Cherry"){
		    	credits += 2000;
	            msg = "You win 2000 credits!";
	            color = "green";
		    }
		    else if(slot1=="7"){
		    	credits += 150;
	            msg = "You win 150 credits!";
	            color = "green";
		    }
		    else if(slot1=="3xBAR"){
		    	credits += 50;
	            msg = "You win 50 credits!";
	            color = "green";
		    }
		    else if(slot1=="2xBAR"){
		    	credits += 20;
	            msg = "You win 20 credits!";
	            color = "green";
		    }
		    else if(slot1=="BAR"){
		    	credits += 10;
	            msg = "You win 10 credits!";
	            color = "green";
		    }
	    	    
		}
		else if(slot7 == slot8 && slot8 == slot9){
			
			if(slot7=="Cherry"){
		    	credits += 4000;
	            msg = "You win 4000 credits!";
	            color = "green";
		    }
		    else if(slot7=="7"){
		    	credits += 150;
	            msg = "You win 150 credits!";
	            color = "green";
		    }
		     else if(slot7=="3xBAR"){
		    	credits += 50;
	            msg = "You win 50 credits!";
	            color = "green";
		    }
		    else if(slot7=="2xBAR"){
		    	credits += 20;
	            msg = "You win 20 credits!";
	            color = "green";
		    }
		    else if(slot7=="BAR"){
		    	credits += 10;
	            msg = "You win 10 credits!";
	            color = "green";
		    }
		    else if(slot7.indexOf("BAR") !== -1 && slot8.indexOf("BAR") !== -1 && slot9.indexOf("BAR") !== -1 ){
		    	credits += 5;
	            msg = "You win 5 credits!";
	            color = "green";
		    }
		}

		else if(slot1.indexOf("BAR") !== -1 && slot2.indexOf("BAR") !== -1 && slot3.indexOf("BAR") !== -1 ){
	    	credits += 5;
            msg = "You win 5 credits!";
            color = "green";
		}

		else if(slot7.indexOf("BAR") !== -1 && slot8.indexOf("BAR") !== -1 && slot9.indexOf("BAR") !== -1 ){
	    	credits += 5;
            msg = "You win 5 credits!";
            color = "green";
		}

    }

    else if(position == "center"){
    	if (slot4 == slot5 && slot5 == slot6)
		{
		    if(slot4=="Cherry"){
		    	credits += 1000;
	            msg = "You win 1000 credits!";
	            color = "green";
		    }
		    else if(slot4=="7"){
		    	credits += 150;
	            msg = "You win 150 credits!";
	            color = "green";
		    }
		    else if(slot4=="3xBAR"){
		    	credits += 50;
	            msg = "You win 50 credits!";
	            color = "green";
		    }
		    else if(slot4=="2xBAR"){
		    	credits += 20;
	            msg = "You win 20 credits!";
	            color = "green";
		    }
		    else if(slot4=="BAR"){
		    	credits += 10;
	            msg = "You win 10 credits!";
	            color = "green";
		    }
		    else if(slot4.match("BAR") && slot5.match("BAR") && slot6.match("BAR")){
		    	credits += 5;
	            msg = "You win 5 credits!";
	            color = "green";
		    }
		}

		else if(slot4.indexOf("BAR") !== -1 && slot5.indexOf("BAR") !== -1 && slot6.indexOf("BAR") !== -1 ){
	    	credits += 5;
            msg = "You win 5 credits!";
            color = "green";
		}
    }
	


 	// update credits display and win/lose message
	document.getElementById('credits').innerHTML = credits;
        var msgspan = document.getElementById("message");
        msgspan.innerHTML = msg;
        msgspan.style.color = color;

        if ( credits <= 0 )
        {
             alert("You lost all your money!");
        }
}